/*
Copyright © 2019 NAME HERE <EMAIL ADDRESS>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/
package cmd

import (
  "fmt"
  "os"
  "github.com/spf13/cobra"

  "encoding/json"
)

var projectsCmd = &cobra.Command{
  Use:   "projects",
  Short: "Print project list",
  Long: "Print project list",
  Run: func(cmd *cobra.Command, args []string) {
    getProjects()
  },
}

type gitlabProject struct {
  Id int
  Description string
  Name string
}

func requestProjects() ([]byte, error){
  query := map[string]string{
    "private_token": Token,
    "membership": "true",
  }
  resp, err := getRequest(PROTCOL + Host + "/api/v4/" + "projects", query)
  if err != nil {
    return nil, err
  }
  return *resp, nil
}

func getProjects() {
  resp, err := requestProjects()
  if err != nil {
    fmt.Println(err)
    os.Exit(1)
  }

  var projects []gitlabProject
  err = json.Unmarshal(resp, &projects)
  if err != nil {
    fmt.Println(err)
    os.Exit(1)
  }

  output, err := json.Marshal(&projects)
  if err != nil {
    fmt.Println(err)
    os.Exit(1)
  }

  fmt.Println(string(output))
}

func init() {
  rootCmd.AddCommand(projectsCmd)
}