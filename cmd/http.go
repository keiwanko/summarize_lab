package cmd

import (
  "net/http"
  "io/ioutil"
  "errors"
)

type httpClient interface {
  Do(req *http.Request) (*http.Response, error)
}

func getRequest(target string, query map[string]string) (*[]byte, error) {
  client := &http.Client{}
  req, err := http.NewRequest("GET", target, nil)
  q := req.URL.Query()
  for k, v := range query {
    q.Add(k, v)
  }
  req.URL.RawQuery = q.Encode()
  o := `
<Request>
  Header: %v
  URL: %s


  `
  log.Printf(o, req.Header, req.Form, req.URL.String())
  if err != nil {
    return nil, err
  }
  resp, err := client.Do(req)
  if err != nil {
    return nil, err
  }
  defer resp.Body.Close()

  if resp.StatusCode < 200 || resp.StatusCode > 399 {
    return nil, errors.New("Unexpect status code: " + string(resp.StatusCode))
  }
  o = `
<Response>
  ResponseCode: %d
  Header: %v


  `
  log.Printf(o, resp.StatusCode, resp.Header)
  
  buffer, err := ioutil.ReadAll(resp.Body)
  if err != nil {
    return nil, err
  }
  return &buffer, nil
}