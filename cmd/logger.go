package cmd

import (
	stdlog "log"
	"os"
)

type logger interface {
  Printf(format string, args ...interface{})
  Errorf(format string, args ...interface{})
}

type stdLogger struct {
	stderr *stdlog.Logger
	stdout *stdlog.Logger
}

func newStdLogger() *stdLogger {
	return &stdLogger{
		stdout: stdlog.New(os.Stdout, "", 0),
		stderr: stdlog.New(os.Stderr, "", 0),
	}
}

func (l *stdLogger) Printf(format string, args ...interface{}) {
  if Debug {
    l.stdout.Printf(format, args...)
  }
}

func (l *stdLogger) Errorf(format string, args ...interface{}) {
	l.stderr.Printf(format, args...)
}